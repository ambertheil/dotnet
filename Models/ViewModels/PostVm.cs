using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using MusicDating.Models.Entities;

namespace MusicDating.Models.ViewModels
{

    public class PostVm
    {
        public IEnumerable<Post> Posts { get; set; }
        public SelectList Instruments { get; set; }
        public string InstrumentName { get; set; }

    }
}