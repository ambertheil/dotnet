using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using MusicDating.Models.Entities;

namespace MusicDating.Models.ViewModels
{
    public class ProfileVm
    {
        public ApplicationUser ApplicationUser { get; set; }
        public IEnumerable<UserInstrument> UserInstruments { get; set; }

        public IEnumerable<Post> Posts { get; set; }

    }
}