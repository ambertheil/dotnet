using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using MusicDating.Models.Entities;

public class ApplicationUser : IdentityUser
{
    public DateTime DateCreated { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string ProfileDescription { get; set; }
    public ICollection<UserInstrument> UserInstruments { get; set; }

    public ICollection<Post> Posts { get; set; }
}

