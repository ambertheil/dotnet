using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusicDating.Models.Entities
{
    public class Post
    {
        public int PostId { get; set; }
        public string EnsembleName { get; set; }
        public string Description { get; set; }

        //Navigation property
        public Instrument Instrument { get; set; }
        public int InstrumentId { get; set; }
        public int Level { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

    }
}