using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MusicDating.Models.Entities
{
    public class Genre
    {
        public int GenreId { get; set; }
        public string Name { get; set; }
        public ICollection<GenreEnsemble> GenreEnsemble { get; set; }

        public ICollection<UserInstrumentGenre> UserInstrumentGenres { get; set; }

    }
}