using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MusicDating.Models;
using MusicDating.Data;
using Microsoft.EntityFrameworkCore;
using MusicDating.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;



namespace MusicDating.Models.Services
{
    public class PostServices
    {

        public async static Task<PostVm> SearchForPost(
            ApplicationDbContext _context, string instrumentName)
        {
            //create a view model selectlist with instruments + userInstruments (users)

            // I am showing all instruments in the database. Some of you found (only) the instruments that are in use by musicians.

            // Get all genres in use by userinstruments
            /*   var genreQuery = from x in _context.UserInstrumentGenres.Include(x => x.Genre)
                               select x.Genre; */

            // do some coding - filter users to only display those that play the instrument
            var posts = from x in _context.Posts.Include(x => x.Instrument).Include(y => y.ApplicationUser)
                        select x;

            if (!String.IsNullOrEmpty(instrumentName))
            {
                posts = posts.Where(x => x.Instrument.Name == instrumentName);
            }
            /*   if (genreId != 0)
              {
                  users = from u in users
                          from g in u.UserInstruments
                          from ge in g.UserInstrumentGenres
                          where ge.GenreId == genreId
                          select u;
              } */


            var viewmodel = new PostVm()
            {
                Posts = await posts.ToListAsync(),
                // Genres = new SelectList(await genreQuery.Distinct().ToListAsync(), "GenreId", "Name"),
                Instruments = new SelectList(await _context.Instruments.ToListAsync(), "Name", "Name"),
                InstrumentName = instrumentName,
                // GenreId = genreId
            };

            return viewmodel;
        }
    }

}
