using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MusicDating.Models;
using MusicDating.Data;
using Microsoft.EntityFrameworkCore;
using MusicDating.Models.Entities;
using MusicDating.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using MusicDating.Models.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace MusicDating.Controllers
{
    public class ProfileController : Controller
    {
        // Access to the database
        private readonly ApplicationDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;

        public ProfileController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;

        }

        [Authorize]
        public async Task<IActionResult> Index()
        {

            var user = await _userManager.GetUserAsync(User);
            // var Posts = await _context.Posts.Where(x => x.Id == user.Id).Include(x => x.EnsembleName).Include(x => x.Description).Include(y => y.Instrument).ThenInclude(y => y.Name).ToListAsync();
            var vm = new ProfileVm()
            {
                ApplicationUser = user,
                UserInstruments = await _context.UserInstruments.Where(y => y.ApplicationUser.Id == user.Id).Include(x => x.Instrument).Include(y => y.UserInstrumentGenres).ThenInclude(y => y.Genre).ToListAsync(),
                Posts = await _context.Posts.Where(x => x.ApplicationUser.Id == user.Id).Include(x => x.Instrument).ToListAsync(),

            };

            return View(vm);

        }

        public async Task<IActionResult> Edit()
        {

            ApplicationUser user = await _userManager.GetUserAsync(User);

            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("FirstName,LastName,ProfileDescription")] ApplicationUser applicationUser)
        {

            ApplicationUser user = await _userManager.GetUserAsync(User);
            user.FirstName = applicationUser.FirstName;
            user.LastName = applicationUser.LastName;
            user.ProfileDescription = applicationUser.ProfileDescription;

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationUserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);

        }
        private bool ApplicationUserExists(string id)
        {
            return _context.ApplicationUsers.Any(e => e.Id == id);
        }

    }
}