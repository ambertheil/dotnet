

using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MusicDating.Models.Entities;

namespace MusicDating.Data
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        // This means that EF (Entity Framework) will create a table called Instrument based
        // on our Instrument class.
        public DbSet<Instrument> Instruments { get; set; }

        // This means that EF (Entity Framework) will create a table called Instrument based
        // on our Instrument class.
        public DbSet<MusicDating.Models.Entities.Agent> Agent { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); // Only needed when inheriting from IndentityDbContext.
            // Call base functionality from the OnModelCreating method in the IdentityDbContext Class.

            //App user
            modelBuilder.Entity<ApplicationUser>().HasData(
                new ApplicationUser { Id = "1", DateCreated = new System.DateTime(2020, 12, 24), FirstName = "Simone", LastName = "Grauer", UserName = "Simo085f@kea.dk", Email = "Simo085f@kea.dk", PasswordHash = "Test1234!", ProfileDescription = "Mit navn er simone hehe" },
                new ApplicationUser { Id = "2", DateCreated = new System.DateTime(2020, 10, 12), FirstName = "Thea", LastName = "Theasen", UserName = "Thea@kea.dk", Email = "Thea@kea.dk" }
            );

            modelBuilder.Entity<GenreEnsemble>()
                .HasKey(ge => new { ge.GenreId, ge.EnsembleId });
            modelBuilder.Entity<GenreEnsemble>()
                .HasOne(ge => ge.Genre)
                .WithMany(g => g.GenreEnsemble)
                .HasForeignKey(ge => ge.GenreId);
            modelBuilder.Entity<GenreEnsemble>()
                .HasOne(ge => ge.Ensemble)
                .WithMany(e => e.GenreEnsemble)
                .HasForeignKey(ge => ge.EnsembleId);

            modelBuilder.Entity<UserInstrument>()
                .HasKey(ge => new { ge.Id, ge.InstrumentId });
            modelBuilder.Entity<UserInstrument>()
                .HasOne(ge => ge.ApplicationUser)
                .WithMany(g => g.UserInstruments)
                .HasForeignKey(ge => ge.Id);
            modelBuilder.Entity<UserInstrument>()
                .HasOne(ge => ge.Instrument)
                .WithMany(e => e.UserInstruments)
                .HasForeignKey(ge => ge.InstrumentId);

            modelBuilder.Entity<UserInstrumentGenre>()
                .HasKey(bc => new { bc.UserInstrumentGenreId });
            modelBuilder.Entity<UserInstrumentGenre>()
                .HasOne(bc => bc.Genre)
                .WithMany(b => b.UserInstrumentGenres)
                .HasForeignKey(bc => bc.GenreId);
            modelBuilder.Entity<UserInstrumentGenre>()
                .HasOne(bc => bc.UserInstrument)
                .WithMany(c => c.UserInstrumentGenres)
                .HasForeignKey(bc => new { bc.Id, bc.InstrumentId });


            //Test data
            // Add data - instruments
            modelBuilder.Entity<Instrument>().HasData(
                new Instrument { InstrumentId = 4, Name = "Trumpet" },
                new Instrument { InstrumentId = 6, Name = "Piano" },
                new Instrument { InstrumentId = 8, Name = "Guitar" }
            );
            // Add data userinstruments
            modelBuilder.Entity<UserInstrument>().HasData(
                new UserInstrument { Id = "1", InstrumentId = 4, Level = 3 },
                new UserInstrument { Id = "2", InstrumentId = 6, Level = 5 },
                new UserInstrument { Id = "c09eddee-85aa-4469-961c-c38714f2b894", InstrumentId = 8, Level = 1 }
            );
            modelBuilder.Entity<Genre>().HasData(
                new Genre { GenreId = 1, Name = "Rock" },
                new Genre { GenreId = 2, Name = "Pop" }
            );
            modelBuilder.Entity<Ensemble>().HasData(
                new Ensemble { EnsembleId = 1, Name = "U2", CoverImage = "hello.png", Description = "Rockband" },
                new Ensemble { EnsembleId = 2, Name = "Take That", CoverImage = "take.png", Description = "Pop band" }
            );
            modelBuilder.Entity<GenreEnsemble>().HasData(
                new GenreEnsemble { GenreId = 1, EnsembleId = 1 },
                new GenreEnsemble { GenreId = 2, EnsembleId = 2 }
            );

            modelBuilder.Entity<UserInstrumentGenre>().HasData(
                new UserInstrumentGenre { UserInstrumentGenreId = 1, Id = "1", GenreId = 1, InstrumentId = 4 },
                new UserInstrumentGenre { UserInstrumentGenreId = 2, Id = "2", GenreId = 2, InstrumentId = 6 },
                new UserInstrumentGenre { UserInstrumentGenreId = 3, Id = "c09eddee-85aa-4469-961c-c38714f2b894", GenreId = 2, InstrumentId = 8 }
            );

            modelBuilder.Entity<Post>().HasData(
                new Post { PostId = 1, EnsembleName = "Musikerne", Description = "vi er et ensemble", InstrumentId = 4, Level = 2, Id = "1" },
                new Post { PostId = 2, EnsembleName = "Et ensemble", Description = "nyt ensemble", InstrumentId = 6, Level = 3, Id = "2" },
                new Post { PostId = 3, EnsembleName = "Ambers orkester", Description = "Søger pianist", InstrumentId = 6, Level = 3, Id = "c09eddee-85aa-4469-961c-c38714f2b894" }
            );
        }

        public DbSet<MusicDating.Models.Entities.Ensemble> Ensemble { get; set; }
        public DbSet<MusicDating.Models.Entities.Genre> Genre { get; set; }
        public DbSet<UserInstrument> UserInstruments { get; set; }
        public DbSet<UserInstrumentGenre> UserInstrumentGenres { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Post> Posts { get; set; }
    }
}
