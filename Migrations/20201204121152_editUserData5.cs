﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class editUserData5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserInstruments",
                keyColumns: new[] { "Id", "InstrumentId" },
                keyValues: new object[] { "c09eddee-85aa-4469-961c-c38714f2b894", 6 });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "LockoutEnabled", "SecurityStamp" },
                values: new object[] { "515ab0b1-ecc3-461e-8052-2dc3c0302c94", false, false, "c86a7d53-5030-4045-ab7d-a1beeb6951fe" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "0866c4cf-2cd3-46c2-a100-0be042f22437", "67af0a90-9018-41b3-a11e-b6834048d365" });

            migrationBuilder.InsertData(
                table: "Instruments",
                columns: new[] { "InstrumentId", "Name" },
                values: new object[] { 8, "Guitar" });

            migrationBuilder.InsertData(
                table: "UserInstruments",
                columns: new[] { "Id", "InstrumentId", "Level" },
                values: new object[] { "c09eddee-85aa-4469-961c-c38714f2b894", 8, 1 });

            migrationBuilder.InsertData(
                table: "UserInstrumentGenres",
                columns: new[] { "UserInstrumentGenreId", "GenreId", "Id", "InstrumentId" },
                values: new object[] { 3, 2, "c09eddee-85aa-4469-961c-c38714f2b894", 8 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserInstrumentGenres",
                keyColumn: "UserInstrumentGenreId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "UserInstruments",
                keyColumns: new[] { "Id", "InstrumentId" },
                keyValues: new object[] { "c09eddee-85aa-4469-961c-c38714f2b894", 8 });

            migrationBuilder.DeleteData(
                table: "Instruments",
                keyColumn: "InstrumentId",
                keyValue: 8);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "LockoutEnabled", "SecurityStamp" },
                values: new object[] { "334317a5-1967-45a4-b1c5-2fa20667b966", true, true, "c153d993-91dc-4ce3-ab33-61e1130c171e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "5a562d8f-24e6-48a8-ae2f-cb988fe63dff", "940f11b8-1815-46f2-9edd-beb973abb6de" });

            migrationBuilder.InsertData(
                table: "UserInstruments",
                columns: new[] { "Id", "InstrumentId", "Level" },
                values: new object[] { "c09eddee-85aa-4469-961c-c38714f2b894", 6, 5 });
        }
    }
}
