﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class editUserData2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "SecurityStamp" },
                values: new object[] { "16950e7d-9489-4f4c-9b08-6e6e86f17e77", true, "0948dbec-d49f-4c7b-bdf4-360879b75ca1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "43ec2d4f-9eeb-4919-b3f0-f0d5ffcf38ec", "3274bec4-6d99-4426-8c7a-5964ce0698d4" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "SecurityStamp" },
                values: new object[] { "bf7df86a-8c9c-4bd3-b7d7-ed284c4c3750", false, "376793b9-4fc6-455c-87ad-1de67b60ddf7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "440e1e23-c858-4ef4-a49a-efbe747c124b", "c209ffe2-b652-415a-bfa5-f880b5051551" });
        }
    }
}
