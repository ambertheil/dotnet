﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class ProfileDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProfileDescription",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "ProfileDescription", "SecurityStamp" },
                values: new object[] { "0a8f87cb-0a7c-48f2-bee8-835077332060", "Mit navn er simone hehe", "3d0ab511-f640-4a4d-a23b-256f7a260a1c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "ac8d72bd-d866-4fb0-87ac-68b22be43195", "10a4185c-7c62-4258-abf6-84641feb7f2c" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProfileDescription",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4f4976c7-d764-4dba-a2ec-e9b5ab6b2a22", "398e221e-be34-4621-9deb-aa6fadaa0358" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "75b41cbb-fc96-4560-84b3-aa2fdda00b77", "5b5aeac9-2a5d-4ae7-8be7-64ff4a0ce884" });
        }
    }
}
