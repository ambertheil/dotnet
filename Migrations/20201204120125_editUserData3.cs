﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class editUserData3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "LockoutEnabled", "SecurityStamp" },
                values: new object[] { "c1b9b49e-b8be-43b0-a9be-b71ad738eca0", true, "a4de2f86-fef5-4a45-8266-6dd603515edf" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "93e3d2a4-b92f-4f4c-8278-fffb628ffe43", "36b02e73-f323-4401-b54a-deb00e3af399" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "LockoutEnabled", "SecurityStamp" },
                values: new object[] { "16950e7d-9489-4f4c-9b08-6e6e86f17e77", false, "0948dbec-d49f-4c7b-bdf4-360879b75ca1" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "43ec2d4f-9eeb-4919-b3f0-f0d5ffcf38ec", "3274bec4-6d99-4426-8c7a-5964ce0698d4" });
        }
    }
}
