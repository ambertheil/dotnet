﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class editUserData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "bf7df86a-8c9c-4bd3-b7d7-ed284c4c3750", "Test1234!", "376793b9-4fc6-455c-87ad-1de67b60ddf7" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "440e1e23-c858-4ef4-a49a-efbe747c124b", "c209ffe2-b652-415a-bfa5-f880b5051551" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "da030a76-88a3-4a80-b0dd-6e919eb8767c", "Test1!", "ff55354b-1745-4c38-bd92-b98503cfd35c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8c704c32-057f-425d-9590-555049203777", "7f1ed180-6586-4c83-8060-cb7d60a7b365" });
        }
    }
}
