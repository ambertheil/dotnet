﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class editUserData4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "334317a5-1967-45a4-b1c5-2fa20667b966", "c153d993-91dc-4ce3-ab33-61e1130c171e" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "5a562d8f-24e6-48a8-ae2f-cb988fe63dff", "940f11b8-1815-46f2-9edd-beb973abb6de" });

            migrationBuilder.InsertData(
                table: "UserInstruments",
                columns: new[] { "Id", "InstrumentId", "Level" },
                values: new object[] { "c09eddee-85aa-4469-961c-c38714f2b894", 6, 5 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserInstruments",
                keyColumns: new[] { "Id", "InstrumentId" },
                keyValues: new object[] { "c09eddee-85aa-4469-961c-c38714f2b894", 6 });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "c1b9b49e-b8be-43b0-a9be-b71ad738eca0", "a4de2f86-fef5-4a45-8266-6dd603515edf" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "93e3d2a4-b92f-4f4c-8278-fffb628ffe43", "36b02e73-f323-4401-b54a-deb00e3af399" });
        }
    }
}
