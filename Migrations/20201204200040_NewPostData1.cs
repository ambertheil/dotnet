﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class NewPostData1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "051d83f0-f200-491a-ae3e-a6e219998153", "fc9b5a04-0500-48ee-a38a-dcdaec95705d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8a1ef545-8c42-4eba-a38c-94a36130d968", "fb13bc32-e3a4-4db8-ae7b-5705f8bc8d80" });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "Description", "EnsembleName", "InstrumentId", "Level", "PostId" },
                values: new object[] { "c09eddee-85aa-4469-961c-c38714f2b894", "Søger pianist", "Ambers orkester", 6, 3, 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: "c09eddee-85aa-4469-961c-c38714f2b894");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "515ab0b1-ecc3-461e-8052-2dc3c0302c94", "c86a7d53-5030-4045-ab7d-a1beeb6951fe" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "0866c4cf-2cd3-46c2-a100-0be042f22437", "67af0a90-9018-41b3-a11e-b6834048d365" });
        }
    }
}
