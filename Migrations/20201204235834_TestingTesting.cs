﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class TestingTesting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "4f4976c7-d764-4dba-a2ec-e9b5ab6b2a22", "398e221e-be34-4621-9deb-aa6fadaa0358" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "75b41cbb-fc96-4560-84b3-aa2fdda00b77", "5b5aeac9-2a5d-4ae7-8be7-64ff4a0ce884" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "051d83f0-f200-491a-ae3e-a6e219998153", "fc9b5a04-0500-48ee-a38a-dcdaec95705d" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8a1ef545-8c42-4eba-a38c-94a36130d968", "fb13bc32-e3a4-4db8-ae7b-5705f8bc8d80" });
        }
    }
}
