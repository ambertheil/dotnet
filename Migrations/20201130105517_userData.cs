﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MusicDating.Migrations
{
    public partial class userData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "da030a76-88a3-4a80-b0dd-6e919eb8767c", "Test1!", "ff55354b-1745-4c38-bd92-b98503cfd35c" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "8c704c32-057f-425d-9590-555049203777", "7f1ed180-6586-4c83-8060-cb7d60a7b365" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fc5cb351-3f29-4cc3-8a4c-f4c730e0bfd7", null, "51d61165-ce55-44eb-b8a0-c7044802b0c0" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "69e9ea92-db02-47b1-932a-65d63504ab06", "ad67f9c5-b95b-4374-8719-2af3d662551c" });
        }
    }
}
